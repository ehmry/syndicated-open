{
  pkgs ? import <nixpkgs> { },
  src ? if pkgs.lib.inNixShell then null else pkgs.lib.cleanSource ./.,
}:
with pkgs;
buildNimSbom (finalAttrs: {
  inherit src;
  nativeBuildInputs = [ pkg-config ];
  buildInputs = [
    pcre
  ];
  nimFlags = [ "--define:nimPreviewHashRef" ];
  postInstall = ''
    ln -s $out/bin/open $out/bin/xdg-open
  '';
  meta.mainProgram = "open";
}) ./sbom.json
