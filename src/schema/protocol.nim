
import
  preserves, std/sets

type
  Response* {.preservesRecord: "a".} = object
    `open`*: Open
    `resp`*: Value
  Open* {.preservesRecord: "open".} = object
    `uri`*: string
    `types`*: MediaTypes
  MediaTypes* = HashSet[Symbol]
  Rule* {.preservesTuple.} = object
    `pattern`*: string
    `types`*: MediaTypes
    `entity`* {.preservesEmbedded.}: Value
    `action`*: Value
  Rules* {.preservesRecord: "open-rules".} = object
    `rules`* {.preservesTupleTail.}: seq[Rule]
  Query* {.preservesRecord: "q".} = object
    `open`*: Open
proc `$`*(x: Response | Open | MediaTypes | Rule | Rules | Query): string =
  `$`(toPreserves(x))

proc encode*(x: Response | Open | MediaTypes | Rule | Rules | Query): seq[byte] =
  encode(toPreserves(x))
