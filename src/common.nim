# SPDX-FileCopyrightText: ☭ Emery Hemingway
# SPDX-License-Identifier: Unlicense

import
  std/uri,
  pkg/preserves

proc toPreservesOrNull(s: string): Value=
  if s != "": result = s.toPreserves

proc toPreservesHook*(u: Uri): Value =
  ## Best-effort preservation of the `Uri` type.
  runnableExamples:
    from std/unittest import check
    import std/uri
    import preserves
    var u = parseUri"https://bidstonobservatory.org"
    check $(u.toPreserves) == """<uri "https" [#f #f "bidstonobservatory.org" #f] #f #f #f>"""
  initRecord("uri",
      u.scheme.toPreserves,
      toPreserves([
          u.username.toPreservesOrNull,
          u.password.toPreservesOrNull,
          u.hostname.toPreservesOrNull,
          u.port.toPreservesOrNull,
        ]),
      u.path.toPreservesOrNull,
      u.query.toPreservesOrNull,
      u.anchor.toPreservesOrNull,
    )

proc fromPreservesHook*(u: var Uri; pr: Value): bool =
  ## Best-effort preservation of the `Uri` type.
  runnableExamples:
    import std/uri
    import preserves
    var u: Uri
    doAssert fromPreservesHook(u, parsePreserves"""<uri "https" [#f #f "bidstonobservatory.org" #f] #f #f #f>""")
  if pr.isRecord("uri", 5):
    result = fromPreserves(u.scheme, pr[0])
    if result:
      if pr[1].isSequence and pr[1].sequence.len == 4:
        discard fromPreserves(u.username, pr[1][0])
        discard fromPreserves(u.password, pr[1][1])
        discard fromPreserves(u.hostname, pr[1][2])
        discard fromPreserves(u.port, pr[1][3])
      discard fromPreserves(u.path, pr[2])
      discard fromPreserves(u.query, pr[3])
      discard fromPreserves(u.anchor, pr[4])
