# SPDX-FileCopyrightText: ☭ Emery Hemingway
# SPDX-License-Identifier: Unlicense

import
  std/[cmdline, files, paths, sets, streams],
  pkg/cbor,
  pkg/eris,
  pkg/freedesktop_org,
  pkg/syndicate,
  pkg/syndicate/relays,
  ./schema/protocol,
  ./common

from  pkg/eris/cbor_stores import fromCborHook

type Mode = enum msgMode, assMode

proc openErisLink(path: string): Open =
  try:
    var cap: ErisCap
    let
      stream = openFileStream(path)
      data = readCbor(stream)
    stream.close()
    if not cap.fromCborHook(data.seq[0]):
      quit("failed to parse ERIS cap from " & path)
    result.uri = $cap
    result.types.incl data.seq[2].text.Symbol
  except CatchableError:
    quit("failed to parse ERIS link file " & path)

proc toOpen(arg: string): Open =
  let path = Path(arg).absolutePath
  if path.fileExists:
    let path = $path
    result.uri = "file://" & path
    for mt in path.mimeTypeOf:
      if mt == "application/x-eris-link+cbor":
        result = openErisLink(path)
        return
      result.types.incl mt.Symbol
  else:
    result.uri = arg

proc publishUri(turn: Turn; ds: Cap) =
  let outerFacet = turn.facet
  var mode: Mode
  var queryCount: int
  for arg in commandLineParams():
    case arg
    of "-m", "--message": mode = msgMode
    of "-a", "--assert": mode = assMode
    else:
      let open = arg.toOpen.toPreserves
      case mode
      of msgMode:
        message(turn, ds, open)
      of assMode:
        queryCount.inc()
        let h = publish(turn, ds, initRecord("q", open))
        onPublish(turn, ds, matchRecord("a", open.match, grab())) do (resp: Value):
          stderr.writeLine resp
          retract(turn, h)
          queryCount.dec()
          if queryCount < 1:
            turn.withFacet outerFacet:
              turn.stopActor()
  if queryCount < 1:
    queueTurn(turn.facet, externalCause "end", stopActor)

runActor("open") do (turn: Turn):
  resolveEnvironment(turn, publishUri)
