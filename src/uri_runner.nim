# SPDX-FileCopyrightText: ☭ Emery Hemingway
# SPDX-License-Identifier: Unlicense

import
  std/[re, sets, tables],
  pkg/syndicate,
  pkg/syndicate/relays,
  pkg/syndicate/protocols/gatekeeper,
  ./schema/protocol,
  ./common,
  ./private/pkgconfig

# std/re uses dlload and doesn't actually link against PCRE.
{.passC: pkgConfig" --cflags libpcre".}
{.passL: pkgConfig" --libs libpcre".}

type
  Rules {.preservesRecord: "open-handlers".} = object
    rules {.preservesTupleTail.}: seq[Rule]
  Rule {.preservesTuple.} = object
    ## Same as the protocol except with a compiled regex.
    regex: Regex
    types: MediaTypes
    entity: Cap
    action: Value

  MatchEntity {.final.} = ref object of EntityObj
    ds: Cap
    facets: Table[Handle, Facet]
    rules: Table[Handle, Rules]

proc fromPreservesHook(regex: var Regex; pr: Value): bool =
  if pr.isString:
    try:
      regex = pr.string.re({reIgnoreCase, reStudy})
      result = true
    except CatchableError:
      debugEcho "failed to compile regex ", pr

block:
  var regex: Regex
  assert fromPreservesHook(regex, ".*".toPreserves)

proc rewrite(result: var Value; uri: string; regex: Regex) =
  # Apply regex replacements to all strings within a Preserves `Value`.
  result.apply do (pr: var Value):
    if pr.isString:
      try:
        pr.string = replacef(uri, regex, pr.string)
      except CatchableError: discard

proc dispatch(turn: Turn; rule: Rule; open: Open): bool =
  if match(open.uri, rule.regex) and
      (len(rule.types) == 0 or len(open.types) == 0 or len(rule.types * open.types) > 0):
    var action = rule.action
    try:
      rewrite(action, open.uri, rule.regex)
      message(turn, rule.entity, action)
      return true
    except CatchableError:
      stderr.writeLine "failed to rewrite into ", rule.action

proc dispatch(turn: Turn; e: MatchEntity; open: Open): bool =
  var
    i: int
    progress = true
  while progress and not result:
    progress = false
    for e in e.rules.values:
      if i < e.rules.len:
        # Loop over each rule list and try the Nth entry.
        result = result or dispatch(turn, e.rules[i], open)
        progress = true
    inc i

proc dispatchAssertion(turn: Turn; e: MatchEntity; req: Value; open: Open): Value =
  if dispatch(turn, e, open):
    initRecord("a".toSymbol, req, initRecord("ok"))
  else:
    initRecord("a".toSymbol, req, initRecord("error", "no handler matched".toPreserves))

proc installMatchEntity(turn: Turn; ds: Cap) =
  proc a(turn: Turn; e: Entity; v: Value; h: Handle) =
    let e = MatchEntity(e)
    block:
      let open = v{0}.preservesTo Open
      if open.isSome:
        turn.withNewFacet:
          e.facets[h] = turn.facet
          let resp = dispatchAssertion(turn, e, v[0], open.get)
          publish(turn, e.ds, resp)
        return
    block:
      let o = v{0}.preservesTo protocol.Rules
      if o.isSome:
        var r: Rules
        r.rules.setLen(o.get.rules.len)
        for i in 0..r.rules.high:
          doAssert r.rules[i].fromPreserves(o.get.rules[i].toPreserves)
        e.rules[h] = r
        return
    echo "unhandled assertion ", v

  proc r(turn: Turn; e: Entity; h: Handle) =
    let e = MatchEntity(e)
    e.rules.del(h)
      # Remove a rule list by its assertion handle.
    var f: Facet
    if e.facets.pop(h, f):
      turn.withFacet(f):
        turn.stopFacet()

  proc m(turn: Turn; e: Entity; v: Value) =
    let e = MatchEntity(e)
    let o = v.preservesTo Open
      # Step to the first element of observe bindings.
    if o.isSome:
      if not dispatch(turn, e, o.get):
        stderr.writeLine "no match for ", o.get

  let cap = turn.facet.newCap:
    MatchEntity(facet: turn.facet, a: a, r: r, m: m, ds: ds)
  let openPat = Open.matchType.grab
  discard observe(turn, ds, openPat, cap)
  discard observe(turn, ds, Query.match({0: openPat}), cap)
  discard observe(turn, ds, matchRecord("open-rules").grab(), cap)
    # Observe the fields of Open and ActionHandler.

proc stdioRoute: Route =
  result.transports.add Stdio().toPreserves

runActor("main") do (turn: Turn):
  resolve(turn, stdioRoute(), installMatchEntity)
    # Use the intitial dataspace on the client-side of the stdio transport
    # for installing rules and observing open messages.
