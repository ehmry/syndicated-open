# Syndicated Open

An [open command](https://en.wikipedia.org/wiki/Open_(process)) implementation that uses [Syndicate](http://syndicate-lang.org/) and PCRE pattern matching to open URIs.

There are two utilites, `open` and `uri-runner`. The former connects to a shared Syndicate dataspace using a route at `$SYNDICATE_ROUTE` and has no other configuration. The `uri-runner` component is intended to be managed by the [Syndicate server](https://git.syndicate-lang.org/syndicate-lang/syndicate-rs).

The `open` utility recognizes the `-m` and `-a` flags for specifiying that open requests are made by message or assertion.
Making requests by fire-and-forget messages is the default.

Matching patterns to actions is done with `open-rules` records.
An `open-rules` record may contain rules.
An open request is matched against the first rule in every `open-rules` record.
If no matches are found then matches are attempted for every second rule, every third rule, and so on until all records have been searched without finding a match.

```preserves
<open-rules ["foo://(.*):(.*)" #{} $entity <krempel ["--host=$1" "--port=$2"]>]>
```
In the preceding example the URI `foo://bar:42` would cause the message `<krempel ["--host=bar" "--port=42"]>` to be sent to `$entity`.

See [handlers-example.pr](./handlers-example.pr) for more information.

The [protocol.nim](./src/schema/protocol.nim) file is generated from the [protocol.prs](./protocol.prs) schema, a [Tupfile](https://gittup.org/tup/) file is provided to do this.

## TODO:
- [x] Media type analysis
- [x] Open with `<q>` and `<a>`
- [ ] Embed stdout of open command
- [ ] URI rewrite rules
